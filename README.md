# Jeûne Ecolo

# Évaluation compétences 1, 2, 3, 5, 6 et 7
Mise en place d'un site de défis écologiques :
* Maquetter une application
* Réaliser une interface utilisateur web statique et adaptable
* Développer une interface utilisateur web dynamique
* Créer une base de données
* Développer les composants d’accès aux données
* Développer la partie back-end d’une application web ou web mobile


## Objectifs
* Modéliser l'application
* Réaliser l'application (front + back)

## Réalisation
* Durée : A rendre avant le 10 octobre minuit
* En solo  
A partir du moment où vous rendez ce que vous avez fait avant le 10 octobre minuit, vous pouvez continuer à travailler dessus jusqu'au dimanche minuit. 


## Rendu :
* Les wireframes
* Le code source du site
* Le lien vers le site en production
* La modélisation UML
* La modélisation de la BDD
* Le script de création de la BDD + les données initiales
* Un PPP à la fin du premier jour d'éval. 

## Projet
### Général
Le projet consiste à réaliser une application permettant à des lycéens de se lancer des défis écologiques : 
* Ne pas acheter d'équipements neufs
* Ne pas manger de viande
* Prendre les transports en commun
* Trier ses déchets
* (et plein d'autres)

### User stories
* En tant que visiteur, je peux me créer un compte sur le site. 
* En tant que visiteur, je peux me connecter au site. 
* En tant que visiteur, j'ai accès à une procédure pour régénérer mon mot de passe en cas d'oubli. 
* En tant qu'utilisateur, je peux regarder les défis disponibles. 
* En tant qu'utilisateur, je peux consulter chaque défi, je vois son intitulé, un résumé, des conseils laissés par d'autres utilisateurs ainsi que le nombre de personnes qui se sont lancées. 
* En tant qu'utilisateur, je peux m'ajouter sur un défi.
* En tant qu'utilisateur, je peux ajouter un conseil sur un défi.
* En tant qu'utilisateur, je peux afficher mon profil avec l'ensemble de mes défis en cours et passés. 
* En tant qu'utilisateur, je peux partager la liste de mes défis sur les réseaux sociaux. 
* En tant qu'administrateur, j'ai un CRUD pour les défis. 
* En tant qu'administrateur, je peux consulter la liste des utilisateurs.
* En tant qu'administrateur, je peux bannir un utilisateur. 
* En tant qu'utilisateur, je peux me retirer d'un défi. 

### Consignes
* Le client n'a pas d'idée arrêtée sur la charte graphique du moment que c'est joli et responsible.
* Il vous recommande de jeter un oeil aux applis mobiles : 90 jours, WAG et Ocean's Zero. 
* Il aimerait pouvoir accéder à ses propres défis lorsqu'il n'est pas connecté à Internet. 
* Si le site n'est pas éco-conçu, le client sera triste.

