let express = require("express");
let app = express();
let bodyParser = require("body-parser");
let mysql = require("mysql");
const bcrypt = require("bcrypt");
const mustacheExpress = require("mustache-express");
require("dotenv").config();

// SET
// app.set('view engine', 'pug');
// // Initialisation mustache
// app.engine('mustache', mustacheExpress());

// app.set('view engine', 'mustache');
// app.set('views', __dirname + '/views');
// USE
app.use("/static", express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Connect to database
const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: "jeune-ecolo"
});

db.connect(err => {
  if (err) {
    throw err;
  }
  console.log("MySql is Connected...");
});

// AFFICHER LES DEFIS
app.get("/accueil", function(req, res) {
  var sql = `
      SELECT  
          title, 
          content,
          publication, 
          users.id AS users_id, 
      FROM défis 
      
  `;

  db.query(sql, function(error, articles, fields) {
    if (error) throw error;
    console.log(articles);
    res.render("accueil", { articles: articles });
  });
});

// POST

// // AJOUT DES DEFIS
// var sql = "INSERT INTO défis (title,content) VALUES ?";
// var values = [
//   ['Trier ses déchets', 'Obj: Respecter le tri sélectif et le recyclage des déchets'],
//   ['Zéro plastique', 'Acheter en moins, ça pollue. Autant acheter du verre :) '],
//   ["Moins d'animaux dans l'assiette", "Ne mangez pas plus de viandes qu'il n'y a d'animaux"],
//   ['Bio', "Mangez bio, c'est mieux ! "],
//   ['Vélo pour tous', "Roulez à vélo c'est plus écolo"],
//   ['Moins de voitures plus de transport', "Afin d'éviter de polluer les grandes villes."]
// ];
// db.query(sql, [values], function (err, result) {
//   if (err) throw err;
//   console.log("Number of records inserted: " + result.affectedRows);
// });
// db.query("SELECT * FROM customers", function (err, result, fields) {
//   if (err) throw err;
//   console.log(result);
// });

// ADD DEFI
app.post("/adddefi/", (req, res) => {
  // AJOUT DES DEFIS
  var sql = "INSERT INTO défis (title,content) VALUES ?";
  var values = [
    [
      "Trier ses déchets",
      "Obj: Respecter le tri sélectif et le recyclage des déchets"
    ],
    [
      "Zéro plastique",
      "Acheter en moins, ça pollue. Autant acheter du verre :) "
    ],
    [
      "Moins d'animaux dans l'assiette",
      "Ne mangez pas plus de viandes qu'il n'y a d'animaux"
    ],
    ["Bio", "Mangez bio, c'est mieux ! "],
    ["Vélo pour tous", "Roulez à vélo c'est plus écolo"],
    [
      "Moins de voitures plus de transport",
      "Afin d'éviter de polluer les grandes villes."
    ]
  ];
  db.query(sql, [values], function(err, result) {
    if (err) throw err;
    console.log("Number of records inserted: " + result.affectedRows);
  });
  db.query("SELECT * FROM customers", function(err, result, fields) {
    if (err) throw err;
    console.log(result);
  });
  res.send(__dirname + "/public/defis.html");
});

// ADD CONSEIL
app.post("/addconseil/", (req, res) => {
  console.log(req.body);
  let conseil = { content: req.body.content };
  let sql = "INSERT INTO conseils SET ?";
  let query = db.query(sql, conseil, (err, result) => {
    if (err) {
      throw err;
    }
    console.log(result);
    res.sendFile(__dirname + "/public/defis.html");
  });
});

// ADD USER
app.post("/adduser/", (req, res) => {
  // Cryptage du pwd

  const saltRounds = 10;
  const pwd = req.body.pwd;
  bcrypt.genSalt(saltRounds, function(err, salt) {
    bcrypt.hash(pwd, salt, function(err, hash) {
      console.log(hash);
      let users = { mail: req.body.mail, pwd: hash };
      let sql = "INSERT INTO users SET ?";
      let query = db.query(sql, users, (err, result) => {
        if (err) {
          throw err;
        }
        console.log(result);
        res.sendFile(__dirname + "/public/index.html");
      });
    });
  });
});

// CONNEXION
app.post("/login/", (req, res) => {
  let mail = req.body.mail;
  let pwd = req.body.pwd;
  db.query(
    'SELECT * FROM users WHERE mail = "' + mail + '"',
    (err, result, fields) => {
      console.log(result);
      if (typeof result[0] == "object") {
        let bdd = result[0].pwd;
        console.log(pwd);
        console.log(bdd);
        bcrypt.compare(pwd, bdd, function(err, ok) {
          console.log(ok);
          if (ok == false) {
            res.sendFile(__dirname + "/public/index.html");
          } else {
            res.sendFile(__dirname + "/public/accueil.html");
          }
        });
      } else {
        res.sendFile(__dirname + "/public/index.html");
      }
    }
  );
});
// GET
app.get("/", function(req, res) {
  res.sendFile(__dirname + "/public/register.html");
});
// // GET
// app.get("/", (req, res) => {
//     // res.setHeader('Content-Type', 'text/html');
//     // res.writeHead(200, {'Content-Type': 'text/html'});
//     res.render("pages/index");
//   });

// Ecoute serveur
app.listen(8080, () => {
  console.log("Serveur en marche ");
});
