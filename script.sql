-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema jeune-ecolo
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema jeune-ecolo
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `jeune-ecolo` DEFAULT CHARACTER SET utf8 COLLATE utf8_esperanto_ci ;
USE `jeune-ecolo` ;

-- -----------------------------------------------------
-- Table `jeune-ecolo`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jeune-ecolo`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mail` VARCHAR(100)NOT NULL,
  `pwd` VARCHAR(255)NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB

DEFAULT CHARACTER SET = utf8
COLLATE = utf8_esperanto_ci;


-- -----------------------------------------------------
-- Table `jeune-ecolo`.`défis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jeune-ecolo`.`défis` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45)NOT NULL,
  `content` LONGTEXT NOT NULL,
  `publication` TIMESTAMP NOT NULL  CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `users_id`),
  INDEX `fk_défis_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_défis_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `jeune-ecolo`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_esperanto_ci;


-- -----------------------------------------------------
-- Table `jeune-ecolo`.`conseils`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jeune-ecolo`.`conseils` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `content` MEDIUMTEXT NOT NULL,
  `publication` TIMESTAMP NOT NULL  CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `défis_id` INT(11) NOT NULL,
  `défis_users_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `défis_id`, `défis_users_id`, `users_id`),
  INDEX `fk_conseils_défis1_idx` (`défis_id` ASC, `défis_users_id` ASC) VISIBLE,
  INDEX `fk_conseils_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_conseils_défis1`
    FOREIGN KEY (`défis_id` , `défis_users_id`)
    REFERENCES `jeune-ecolo`.`défis` (`id` , `users_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_conseils_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `jeune-ecolo`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_esperanto_ci;


-- -----------------------------------------------------
-- Table `jeune-ecolo`.`users_has_défis`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `jeune-ecolo`.`users_has_défis` (
  `users_id` INT(11) NOT NULL,
  `défis_id` INT(11) NOT NULL,
  PRIMARY KEY (`users_id`, `défis_id`),
  INDEX `fk_users_has_défis_défis1_idx` (`défis_id` ASC) VISIBLE,
  INDEX `fk_users_has_défis_users_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_défis_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `jeune-ecolo`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_défis_défis1`
    FOREIGN KEY (`défis_id`)
    REFERENCES `jeune-ecolo`.`défis` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_esperanto_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
